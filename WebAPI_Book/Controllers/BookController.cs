﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebAPI_Book.Models;

namespace WebAPI_Book.Controllers
{
    [RoutePrefix("api/book")]
    public class BookController : ApiController
    {
        Book[] books = new Book[]
        {
            new Book { Id = 1, Author = "Tolkien", Title = "Hobbit", Price = 100 },
            new Book { Id = 2, Author = "Tolkien", Title = "LOTR", Price = 200 },
            new Book { Id = 3, Author = "Martin", Title = "Game of Thrones", Price = 300 }
        };

        // GET: api/Book
        // without RoutePrefix [Route("api/Book")]
        [Route]
        public IHttpActionResult Get()
        {
            return Ok(books);
        }

        // GET: api/Book/5
        //[Route("api/Book/{id}")]
        [Route("{id:int:range(1,2)}")]
        public IHttpActionResult Get(int id)
        {
            Book book = books.FirstOrDefault(f => f.Id == id);

            if(book == null)
            {
                return NotFound();
            }
            //OK converts books to a ItthpActionResult
            return Ok(book);
        }

        // api/Book/Author/tolkien
        //[Route("api/Book/Author/{author:alpha}")]
        [Route("Author/{author:alpha}")]
        public IHttpActionResult getBookByAuther (string author)
        {
            Book[] bookArray = books.Where(b => b.Author.ToLower().Contains(author.ToLower())).ToArray();
            return Ok(bookArray);
        }


        // GET: api/Book/Title/Hobbit
        //[Route("api/Book/Title/{title:alpha}")]
        [Route("Title/{title:alpha}")]
        public IHttpActionResult GetBookByTitle (string title)
        {
            Book[] book = books.Where(b => b.Title.ToLower().Contains(title.ToLower())).ToArray();
            return Ok(book);
        }

        // POST: api/Book/Create
        //[Route("api/Book/Create")]
        [Route("Create")]
        public IHttpActionResult Post([FromBody]Book newBook)
        {
            List<Book> booklist = books.ToList<Book>();
            newBook.Id = books.Count() + 1;
            booklist.Add(newBook);

            return Ok(booklist);

        }

        // PUT: api/Book/Update/3
        //[Route("api/Book/Update/{id:int}")]
        [Route("Update/{id:int}")]
        public IHttpActionResult Put(int id, [FromBody]Book newBook)
        {
            Book book = books.FirstOrDefault(f => f.Id == id);
            if(book == null)
            {
                return NotFound();
            }

            book.Title = newBook.Title;
            book.Author = newBook.Author;
            book.Price = newBook.Price;

            return Ok(books);
        }

        // DELETE: api/Book/Delete/3
        //[Route("api/Book/Delete/{id:int}")]
        [Route("Update/{id:int}")]
        public IHttpActionResult Delete(int id) 
        {
            Book bookToRemove = books.FirstOrDefault(f => f.Id == id);

            if(bookToRemove == null)
            {
                return NotFound();
            }

            List<Book> bookList = books.ToList<Book>();
            bookList.Remove(bookToRemove);

            return Ok(bookList);

        }
    }
}
